# Gearbox version 4.0

This version is almost the final version of the gearbox
in which the files are reorganized and the gearbox is 
quite functional.

## How the files are organized in this project
At the beginning, there is the main folder (Gearbox
that contains the entire project. Then,

1. Enclosure
    * Here all the major pieces of the gearbox will be.
        This folder will contain pieces that can be
        printed with larger nozzles, have looser 
        tolerances and are exposed to the exterior.
    *  **Top:** The part that lays betwen the motor 
        holder and the first gear stage. It helps to 
        make the motor holder easier to print. It 
        contains a small wall that presses against the 
        gasket in order to make a seal and not let oil 
        get out of the gearbox easily.
    *  **Motor Holder:** This piece as its name implies,
        hoolds the motor with the rest of the gearbox.
    *  **Bottom:** This is the largest part of the
        gearbox. Its purpose is to cover the gears,
        to form a seal with the Top part and to have 
        an output for the motion.
    *  **Gearbox Holder 1 and 2:** These pieces hold the entire
        gearbox to the chasis of the bot.
    
2. Ring Stages
    * Here, all the internal stuff will stay. This parts
        have tighter tolerances and have to be printed 
        with smaller nozzles (0.2mm recommended). This 
        pieces are also smaller than pieces from the 
        Enclosure.
    *  **Components** This folder contains all the 
        smaller pieces that are used by all the 
        different Ring Stages.
        * **Ring:** The ring gear is the outermost gear
            that holds the planets inside and make them
            turn.
        * **Planet:** These gears go between the Ring
            and the Sun. They spin and make the Carrier
            to also spin at a slower rate increasing 
            torque and reducing speed.
        * **Spacers:** These parts make a space between
            Ring gears to at a distance that is optimal
            for the functioning of the gearbox.
        * **Normal Carrier:** This piece connects to the
            planet gears of the previous stage to 
            the sun gear of the current stage.
        * **Final Carrier:** Similar to the Normal 
            Carrier; however, this carrier has space
            for a M3 nut for the output shaft.
        * **Normal Sun:** This is the normal sun gear 
            used by the middle stages. 
        * **First Sun:** This is the sun gear that is
            connected to the shaft of the motor.
        
    * **First Stage:** This is the stage that is
            connected to the shaft of the motor. This 
            may be the stage that wears the most since it is the one that moves the most.
    * **Middle Stage:** This is the stage that goes
        between the first and last stages.
    * **Last Stage:** This stage is the last one. It is
        the stage that experiences the most torque.

## Some information about how to build the gearbox
- The holes in the **Bottom Part** are for **6-32 x 1/2** screws.
- The hex sockets in **Top part** and **Final Carrier** are for **m3** nuts.
- The output hole in the **Bottom Part** is for a **F623zz** bearing.
- The output shaft is a **m3** threaded rod.
- **m3** threaded rods hold the ring gears and spacers together.
- I used glue to:
    - seal the **F623zz** bearing to the **Bottom Part**.
    - maintain nut in **Final Carrier**.
    - maintain nuts in **Top Part**.
    - seal **Motor Holder** and **Top Part** together.
    - seal **Ring gears** after mounting planets and sun.
    - put **Normal Sun** and **Normal Carrier** together.
- I used silicone to make the seal between **Bottom Part** and **Top Part**.

## Printing
I used a 0.2mm nozzle in order to print the gears and I printed them really slowly (velocity of around 15mm/s or less with
acceleration of around 20mm/s/s).

## Freecad workbenches used
- FCGear
- A2 Plus

